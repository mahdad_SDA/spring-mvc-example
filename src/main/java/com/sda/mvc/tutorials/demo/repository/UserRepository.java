package com.sda.mvc.tutorials.demo.repository;

import com.sda.mvc.tutorials.demo.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}