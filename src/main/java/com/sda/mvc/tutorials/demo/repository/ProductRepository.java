package com.sda.mvc.tutorials.demo.repository;

import com.sda.mvc.tutorials.demo.domain.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product,Long> {
}
