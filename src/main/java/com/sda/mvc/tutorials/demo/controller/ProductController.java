package com.sda.mvc.tutorials.demo.controller;

import com.sda.mvc.tutorials.demo.domain.Product;
import com.sda.mvc.tutorials.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class ProductController {

    private final ProductRepository productRepository;

    @Autowired
    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping(value="/products")
    public String showProductList(Product product){
        return "add-product";
    }

    @PostMapping(value="/addProduct")
    public String addProduct(@Valid Product product, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            return "add-product";
        }
        productRepository.save(product);
        model.addAttribute("products",productRepository.findAll());
        return "main";
    }



}
